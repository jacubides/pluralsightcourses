package com.jact.plur.course.pluraop.config;

import com.jact.plur.course.pluraop.service.SimpleService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "com.jact.plur.course.pluraop.service")
@ComponentScan(basePackages = "com.jact.plur.course.pluraop.components")
public class SimpleAspectConfiguration {

    @Bean
    public SimpleService simpleService(){
        return new SimpleService();
    }
}
