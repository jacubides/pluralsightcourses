package co.com.pluralsight.designpatterns.structural.adapter;

public interface Employee {

    String getId();
    String getFirstName();
    String getLastName();
    String getEmail();
}
