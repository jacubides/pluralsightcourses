package co.com.pluralsight.designpatterns.creational.builder;

public class LunchOrderBeanDemo {

    public static void main(String... args) {
        LunchOrderBean.Builder builder = new LunchOrderBean.Builder();


        LunchOrderBean lunchOrderBean = builder
                .bread("Wheat")
                //.condiments("Lettuce")
                .dressing("Mayo")
                .meat("Turkey")
                .build();

        System.out.println(lunchOrderBean.getBread());
        System.out.println(lunchOrderBean.getCondiments());
        System.out.println(lunchOrderBean.getDressing());
        System.out.println(lunchOrderBean.getMeat());
    }
}
