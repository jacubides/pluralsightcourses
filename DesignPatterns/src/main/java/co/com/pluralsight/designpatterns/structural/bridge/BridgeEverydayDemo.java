package co.com.pluralsight.designpatterns.structural.bridge;

import org.apache.derby.jdbc.EmbeddedDriver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class BridgeEverydayDemo {

    public static void main(String... args) {
        try {
            DriverManager.registerDriver(new EmbeddedDriver());

            String dbUrl = "jdbc:derby:memory:pluralsight/singletonDB;create=true";

            Connection conn = DriverManager.getConnection(dbUrl);

            Statement sta = conn.createStatement();

            int executeUpdate = sta.executeUpdate("CREATE TABLE Address(ID INT, StreetName VARCHAR(20), Ciy VARCHAR (20))");
            System.out.println(executeUpdate != 2 ? "Table Created" : "Problems Creating Table");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
