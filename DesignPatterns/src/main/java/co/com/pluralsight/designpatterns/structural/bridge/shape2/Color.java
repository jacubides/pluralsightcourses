package co.com.pluralsight.designpatterns.structural.bridge.shape2;

public interface Color {

     void applyColor();
}
