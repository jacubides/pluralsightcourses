package co.com.pluralsight.designpatterns.structural.bridge.shape1;

public class RedCircle extends Circle {
    @Override
    public void applyColor() {
        System.out.println("Applying red color");
    }
}
