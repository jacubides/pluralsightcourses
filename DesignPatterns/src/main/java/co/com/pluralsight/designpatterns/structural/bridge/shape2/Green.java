package co.com.pluralsight.designpatterns.structural.bridge.shape2;

public class Green implements Color {
    @Override
    public void applyColor() {
        System.out.println("Applying green color");
    }
}
