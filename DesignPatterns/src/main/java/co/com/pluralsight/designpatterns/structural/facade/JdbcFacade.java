package co.com.pluralsight.designpatterns.structural.facade;

import co.com.pluralsight.designpatterns.creational.singleton.DbSingleton;
import org.apache.derby.jdbc.AutoloadedDriver;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class JdbcFacade {

    DbSingleton instance;

    public JdbcFacade() {
        this.instance = DbSingleton.getInstance();
    }

    public int createTable(){
        Connection conn;
        Statement sta;
        int count = 0;
        try{
            conn =  instance.getConnection();

            sta = conn.createStatement();
            count = sta.executeUpdate("CREATE TABLE Address (ID INTEGER, StreetName VARCHAR(20), City VARCHAR(20))");

            sta.close();
            conn.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return count;
    }

    public int insertIntoTable(){
        Connection conn;
        Statement sta;
        int count = 0;
        try{
            conn =  instance.getConnection();

            sta = conn.createStatement();
            count = sta.executeUpdate("INSERT INTO Address (ID,StreetName,City)" +
                    "VALUES (1,'123 soome street','Layton')");

            sta.close();
            conn.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return count;
    }

    public List<Address> getAddresses(){
        Connection conn;
        Statement sta;
        List<Address> addresses = new ArrayList<>();
        try{
            conn =  instance.getConnection();
            sta = conn.createStatement();
            ResultSet rs = sta.executeQuery("SELECT * FROM Address");

            while (rs.next()) {
                Address address = new Address();
                address.setId(rs.getString(1));
                address.setStreetName(rs.getString(2));
                address.setCity(rs.getString(3));

                addresses.add(address);
            }

            rs.close();
            sta.close();
            conn.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return addresses;
    }
}
