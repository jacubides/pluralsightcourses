package com.jact.plur.springjpa;


import com.jact.plur.springjpa.model.Location;
import com.jact.plur.springjpa.repository.LocationJpaRepository;
import com.jact.plur.springjpa.repository.LocationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LocationPersistenceTests {
	@Autowired
	private LocationRepository locationRepository;

	@Autowired
	private LocationJpaRepository locationJpaRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Test
	public void testJpa(){
		List<Location> locations = locationJpaRepository.findAll();
		assertNotNull(locations);
	}

    @Test
    public void testJpaAnd() {
        List<Location> locations = locationJpaRepository.findByStateAndCountry("Utah", "United States");
        assertNotNull(locations);
        assertEquals("Utah", locations.get(0).getState());
    }

    @Test
    public void testJpaOr() {
        List<Location> locations = locationJpaRepository.findByStateOrCountry("Utah", "Utah");
        assertNotNull(locations);

        assertEquals("Utah", locations.get(0).getState());
    }

    @Test
    public void testJpaIsOrEq() {
        List<Location> locations = locationJpaRepository.findByStateIsOrCountryEquals("Utah", "Utah");
        assertNotNull(locations);

        assertEquals("Utah", locations.get(0).getState());
    }

    @Test
    public void testJpaNot() {
        List<Location> locations = locationJpaRepository.findByStateNot("Utah");
        assertNotNull(locations);
        assertNotSame("Utah", locations.get(0).getState());
    }

	@Test
	@Transactional
    public void testSaveAndGetAndDelete() {
		Location location = new Location();
		location.setCountry("Canada");
		location.setState("British Columbia");
        location = locationJpaRepository.saveAndFlush(location);
		
		// clear the persistence context so we don't return the previously cached location object
		// this is a test only thing and normally doesn't need to be done in prod code
		entityManager.clear();

        Location otherLocation = locationJpaRepository.getOne(location.getId());
		assertEquals("Canada", otherLocation.getCountry());
		assertEquals("British Columbia", otherLocation.getState());
		
		//delete BC location now
        locationJpaRepository.delete(otherLocation);
	}

	@Test
    public void testFindWithLike() {
        List<Location> locs = locationJpaRepository.findByStateLike("New%");
        assertEquals(4, locs.size());

        locs = locationJpaRepository.findByStateNotLike("New%");
        assertEquals(46, locs.size());

        locs = locationJpaRepository.findByStateStartingWith("New");
		assertEquals(4, locs.size());

        locs = locationJpaRepository.findByStateIgnoreCaseStartingWith("New");
        assertEquals(4, locs.size());

        locs = locationJpaRepository.findByStateNotLikeOrderByStateAsc("New%");
        assertEquals(46, locs.size());

        Location loc = locationJpaRepository.findFirstByStateIgnoreCaseStartingWith("a");
        assertEquals("Alabama", loc.getState());
	}

	@Test
	@Transactional  //note this is needed because we will get a lazy load exception unless we are in a tx
    public void testFindWithChildren() {
        Location arizona = locationJpaRepository.getOne(3L);
		assertEquals("United States", arizona.getCountry());
		assertEquals("Arizona", arizona.getState());
		
		assertEquals(1, arizona.getManufacturers().size());
		
		assertEquals("Fender Musical Instruments Corporation", arizona.getManufacturers().get(0).getName());
	}
}
