package com.jact.plur.springjpa.repository;

import com.jact.plur.springjpa.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LocationJpaRepository extends JpaRepository<Location, Long> {
    List<Location> findByStateLike(String stateName);

    List<Location> findByStateOrCountry(String state, String country);

    List<Location> findByStateAndCountry(String state, String country);

    List<Location> findByStateIsOrCountryEquals(String state, String country);

    List<Location> findByStateNot(String state);

    List<Location> findByStateNotLike(String state);

    List<Location> findByStateStartingWith(String state);

    List<Location> findByStateIgnoreCaseStartingWith(String state);

    List<Location> findByStateNotLikeOrderByStateAsc(String state);

    Location findFirstByStateIgnoreCaseStartingWith(String stateName);
}
